package com.xxx.nacos.config.demo;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HelloController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    @Value("${msg}")
    private String msg;

    @NacosInjected
    private NamingService namingService;

    HelloController() {
        LOGGER.debug("*** Initialized HelloController ***");
    }

    @GetMapping("/say")
    public ResponseEntity<String> sayHello() {
        LOGGER.debug("Called the sayHello method");
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }

    @GetMapping("/getInstances")
    public List<Instance> getInstances(@RequestParam String serviceName) throws NacosException {
        return namingService.getAllInstances(serviceName);
    }
}
