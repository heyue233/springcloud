package com.xxx.springcloud.nacos.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudNacosConfigApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringcloudNacosConfigApplication.class, args);
  }

}
