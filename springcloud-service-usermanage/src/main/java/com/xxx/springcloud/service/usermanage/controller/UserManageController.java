package com.xxx.springcloud.service.usermanage.controller;

import com.xxx.springcloud.service.usermanage.generated.tables.pojos.User;
import com.xxx.springcloud.service.usermanage.service.UserManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserManageController {

  @Autowired
  private UserManageService userManageService;

  @GetMapping("/add/{name}/{username}/{password}/{birth}")
  public String addUser(@PathVariable String name,
                        @PathVariable String username,
                        @PathVariable String password,
                        @PathVariable String birth) {
    User user = new User(0, name, username, password, birth);
    if (userManageService.add(user) == 1) {
      return "添加成功";
    } else {
      return "添加失败";
    }

  }
}
