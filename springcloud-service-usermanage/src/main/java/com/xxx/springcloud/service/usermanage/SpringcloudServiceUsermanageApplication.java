package com.xxx.springcloud.service.usermanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudServiceUsermanageApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringcloudServiceUsermanageApplication.class, args);
  }

}
