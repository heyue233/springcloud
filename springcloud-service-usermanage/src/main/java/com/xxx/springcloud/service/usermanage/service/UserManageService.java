package com.xxx.springcloud.service.usermanage.service;

import com.xxx.springcloud.service.usermanage.generated.tables.pojos.User;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManageService {

  @Autowired
  private DSLContext context;

  private static final com.xxx.springcloud.service.usermanage.generated.tables.User USER = com.xxx.springcloud.service.usermanage.generated.tables.User.USER;

  public int add(User user) {

    return context.insertInto(USER)
        .columns(USER.NAME, USER.USERNAME, USER.PASSWORD, USER.BIRTH)
        .values(user.getName(), user.getUsername(), user.getPassword(), user.getBirth())
        .execute();
  }
}
