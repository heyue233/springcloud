package com.xxx.springcloud.api.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "gateway")
public class GatewayConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(GatewayConfiguration.class);

    private List<Route> routes;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        LOGGER.info("Test: {}", routes);
        if (CollectionUtils.isEmpty(routes)) {
            return builder.routes().build();
        }
//        routes.forEach(route -> {
//            builder.routes().route(route.getId(), predicateSpec -> predicateSpec
//                            .path(route.getPath())
//                            .uri(route.getUri())
//
//            );
//        });
        builder.routes()
                .route("springcloud-service-login", p -> p
                        .path("/service/login/**")
                        .uri("lb://springcloud-service-login")

        );
        return builder.routes().build();
    }

    public GatewayConfiguration setRoutes(List<Route> routes) {
        this.routes = routes;
        return this;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public static class Route {

        private String id;

        private String uri;

        private String path;

        public String getId() {
            return id;
        }

        public Route setId(String id) {
            this.id = id;
            return this;
        }

        public String getUri() {
            return uri;
        }

        public Route setUri(String uri) {
            this.uri = uri;
            return this;
        }

        public String getPath() {
            return path;
        }

        public Route setPath(String path) {
            this.path = path;
            return this;
        }

        @Override
        public String toString() {
            return "Route{" +
                    "id='" + id + '\'' +
                    ", uri='" + uri + '\'' +
                    ", path='" + path + '\'' +
                    '}';
        }
    }
}
