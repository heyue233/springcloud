package com.xxx.springcloud.service.order.api.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "springcloud-service-login")
public interface LoginApi {

    @GetMapping("/service/login/sayHello")
    String getMessage(@RequestParam String username);
}
