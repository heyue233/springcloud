package com.xxx.springcloud.service.order.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.xxx.springcloud.service.order.api.clients.LoginApi;
import com.xxx.springcloud.service.order.api.clients.RemoteClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RemoteClient remoteClient;

    @Autowired
    private LoginApi loginApi;

    @GetMapping("/sayHello/{message}")
    @HystrixCommand(fallbackMethod = "defaultGetMessage")
    public String sayHello(@PathVariable String message) {
        // 之所以不写实际的ip地址，是因为需要实现负载均衡就必须要写注册中心中的服务名称
        // 因为可能存在多台服务器上部署了同一个Service，这些服务在注册中心的名称都是一样的，只是ip地址不同。
//    restTemplate.getForObject("http://springcloud-service-login/getMessage?message="+ message, String.class)

        // 有服务，能够正确返回
        System.out.println("loginApi: " + loginApi.getMessage(message));
        // 没有该服务，触发“回退”
        System.out.println("remoteClient: " + remoteClient.getMessage(message));

        return "正确返回";
    }

    /**
     * 熔断时回调的方法<br>
     * 该方法：参数个数与参数类型应该与{@code @HystrixCommand}注解的方法一致<br>
     * 并且返回值也需要一致。
     *
     * @param message 参数
     * @return 返回值
     */
    public String defaultGetMessage(String message) {

        return "Hystrix was trigger";
    }

}
