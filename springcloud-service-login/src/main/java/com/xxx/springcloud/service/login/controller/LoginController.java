package com.xxx.springcloud.service.login.controller;

import com.xxx.springcloud.service.login.common.CommonResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @GetMapping("/user/login")
    public CommonResponse login(@RequestParam String username, @RequestParam String password) {

        return new CommonResponse()
                .setCode(200)
                .setData(username)
                .setMessage("登录成功");
    }

    @GetMapping("/sayHello")
    public CommonResponse sayHello(@RequestParam String username) {
        return new CommonResponse()
                .setCode(200)
                .setData(null)
                .setMessage("Hello! " + username);
    }

}
