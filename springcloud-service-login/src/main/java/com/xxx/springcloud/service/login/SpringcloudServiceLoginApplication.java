package com.xxx.springcloud.service.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

import java.net.Socket;
import java.util.Random;

@SpringBootApplication
public class SpringcloudServiceLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudServiceLoginApplication.class, args);
    }
}
